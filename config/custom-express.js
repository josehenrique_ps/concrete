/* 
*/
var express = require('express');
var expressLoad = require('express-load');
var expressValidator = require('express-validator');
var bodyParser = require('body-parser');

module.exports = () => {

    var app = express();

    app.use(bodyParser.urlencoded({extended: true}));
    app.use(bodyParser.json());
    app.use(expressValidator());

    expressLoad('controllers', { cwd: 'app' })
        .then('persistencia')
        .into(app);
        
    
    return app;
}

