var mysql = require('mysql');

function createDBConnection() {
    if (!process.env.NODE_ENV) {
        return mysql.createConnection({
            host: 'localhost',
            user: 'root',
            password: 'root',
            database: 'concrete'
        });
    } else if (process.env.NODE_ENV === 'production') {
    //CLEARDB_DATABASE_URL: mysql://b5ec9bd2af918f:f554b826@us-cdbr-iron-east-05.cleardb.net/heroku_d1010715246903f?reconnect=true
        return mysql.createConnection({
            host: 'us-cdbr-iron-east-05.cleardb.net',
            user: 'b5ec9bd2af918f',
            password: 'f554b826',
            database: 'heroku_d1010715246903f'
        });
    }
}

//Wrapper
module.exports = () => {
    return createDBConnection;
}