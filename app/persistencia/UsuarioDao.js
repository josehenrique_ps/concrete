var sprintf = require('sprintf');

function UsuarioDao(connection) {
    this._connection = connection;
}

UsuarioDao.prototype.salva = function(usuario,callback) {
    let nome = usuario.nome;
    let email = usuario.email;
    let senha = usuario.senha;
    let data_criacao = usuario.data_criacao;
    let token = usuario.token;

    console.log(usuario);

    var sql = sprintf('INSERT INTO usuario '
        + '(nome, email, senha, data_criacao, data_ultimo_login, token) '
        + 'VALUES ("%s", "%s", "%s", NOW(), NOW(), "%s");',
        nome, email, senha, token);

    //inserindo usuário
    this._connection.query(sql, function (err, res) {
        if (err) throw err;

        let usuarioId = res.insertId;
        console.log('ultimo id:', usuarioId);

        //inserindo telefones
        var telefones = usuario.telefones;

        telefones.forEach( (telefone) => {
            var numero = telefone.numero;
            var ddd = telefone.ddd;
            var sqlTelefone = sprintf('INSERT INTO telefone (numero, ddd, id_usuario) VALUES (%s, %s, %d);',
                numero, ddd, usuarioId);
            this._connection.query(sqlTelefone, callback);
        });
    });
}

UsuarioDao.prototype.atualizaDataLogin = function (callback) {
    this._connection.query("update usuario set data_ultimo_login = NOW()", callback);
}

UsuarioDao.prototype.buscaPorEmail = function (email, callback) {
    this._connection.query("select * from usuario where email = ?", [email], callback);
}

UsuarioDao.prototype.buscaPorToken = function (token, callback) {
    this._connection.query("select * from usuario where token = ?", [token], callback);
}

UsuarioDao.prototype.buscaPorId = function (id,callback) {
    this._connection.query("select * from usuario where id = ?",[id],callback);
}

UsuarioDao.prototype.deletaPorId = function (id,callback) {
    this._connection.query("delete usuario where id = ?",[id],callback);
}

module.exports = function(){
    return UsuarioDao;
};