/*
Classe responsável por tratar todas as requisições das funcionalidades
referente a usuário.
*/
var jwt = require('jsonwebtoken'); //pacote usado para criar e verificar os tokens

module.exports = (app) => {

    app.get('/usuario/buscar/:user_id', (req, res) => {
        var token = req.headers.token;
        var user_id = req.params.user_id;
        var connection = app.persistencia.connectionFactory();
        var usuarioDao = new app.persistencia.UsuarioDao(connection);

        usuarioDao.buscaPorToken(token, (erros, resultado) => {
            if (resultado.length === 0) {
                res.status(401).json({ mensagem: "Não autorizado" });
            } else {
                usuarioDao.buscaPorId(user_id, (erros, resultado) => {
                    if (resultado.length === 1) {
                        var usuario = resultado[0];
                        var tokenGravado = usuario.token;
                        if (tokenGravado === token) {
                            var dataAtual = new Date();
                            var tempoDiferenca = dataAtual - usuario.data_ultimo_login;
                            var tempoMinutos = tempoDiferenca / 60000;
                            
                            if (tempoMinutos >= 30) {
                                res.status(401).json({ mensagem: "Sessão inválida" });    
                            } else {
                                res.status(200).json(usuario);
                            }
                        } else {
                            res.status(401).json({ mensagem: "Não autorizado" });
                        }
                    } else {
                        res.status(401).json({ mensagem: "Usuário não encontrado" });
                    }
                });
            }
        });
    });

    app.post('/usuario/sign-in', (req, res) => {
        console.log('sign-in');
        req.assert('email',
            'E-mail é obrigatório').notEmpty();

        req.assert('senha',
            'E-mail é obrigatório').notEmpty();

        var erros = req.validationErrors();

        if (erros) {
            console.log('Erros de validação encontrados.');
            res.status(401).json({ mensagem: "Usuário e/ou senha inválidos" });
            return;
        }

        var usuario = req.body;

        var connection = app.persistencia.connectionFactory();
        var usuarioDao = new app.persistencia.UsuarioDao(connection);

        console.log('user body:' + usuario.email);

        //usuarioDao.buscaPorEmail(usuario.email, (erro, resultado) => {
        buscaPorEmail(usuario.email, (erro, resultado) => {
            console.log('executou');
            if (resultado.length === 0) {
                res.status(401).json({ mensagem: "Usuário e/ou senha inválidos" });
                return;
            } else {
                validarToken(resultado[0], res, (erro, resultado) => {});
            }
        });
    });

    validarToken = (usuario, res, callback) => {

        jwt.verify(usuario.token, 'josehenrique', function(err, decoded) {      
            if (err) {
                console.log('token tes error' + err);
                res.status(400).json('{ mensagem: {' + err + '}');
            } else {
                res.status(200).json(usuario);
            }
        });
    }


    var buscaPorEmail = (email, callback) => {
        console.log('email:' + email);
        var connection = app.persistencia.connectionFactory();
        var usuarioDao = new app.persistencia.UsuarioDao(connection);

        usuarioDao.buscaPorEmail(email, callback);

        connection.end();
    }


    app.post('/usuario/sign-up', (req, res) => {
        
        req.assert('email',
            'E-mail é obrigatório').notEmpty();

        var erros = req.validationErrors();

        if (erros) {
            console.log('Erros de validação encontrados.');
            res.status(400).send(erros);
            return;
        }

        var usuario = req.body;

        //validando existência do usuário
        var connection = app.persistencia.connectionFactory();
        var usuarioDao = new app.persistencia.UsuarioDao(connection);

        buscaPorEmail(usuario.email, (erro, resultado) => {

            if (resultado.length > 0) {
                res.status(409).json({ mensagem: 'E-mail já existente'});
                return;
            }
    
            console.log('processando uma requisicao de um novo usuario');
    
            //gerando o token para o novo usuário
            var token = jwt.sign(usuario, 'josehenrique', {
                expiresIn: 60*30 //o token irá expirar em 30 minutos.
            });
            
            usuario.token = token;
    
            usuarioDao.salva(usuario, (erro, resultado) => {
                if (erro) {
                    console.log('Error ao inserir no banco: ' + erro);
                    res.status(500).send(erro);
                } else {
                    console.log('usuario criado');
                    res.location('/usuario/salva/' 
                        + resultado.insertId);
                    res.status(201).json(usuario);
                }
            });
    
        });
    });

}